# Pretty Diverse Groups (PDG)

Imagine you have a class full of students and you want to divide them into groups.  Ideally, you'd like each group to have a diversity similar to that of the class as a whole.  For example, you wouldn't want to put all of the girls in one group.

If you wanted to maximize the diversity of your groups, you'd be facing the ["Maximally Diverse Groups Problem"](https://www.sciencedirect.com/science/article/abs/pii/S0020025514010263) [(MDGP)](https://grafo.etsii.urjc.es/optsicom/mdgp), which is known to be [NP-hard](https://en.wikipedia.org/wiki/NP-hardness).  That makes it a tough nut to crack.

The program here doesn't aim for "maximal" diversity.  Instead, it aims for "pretty good" diversity, with the thought that this will be good enough for many cases.

## How it works

The program takes an input CSV file describing the properties of each person (or really any type of thing) in a population, and sorts those people into a given number of groups while attempting to make each group as diverse as possible.  Ideally, each group should be a reflection, in miniature, of the distribution of properties in the whole input population.

The input CSV file should have one line per person, with the first field on each line being a unique identifier for that person.  The content of the fields can be anything you like.  For example, one column might contain values like "female", "male", or "other".  Another column might be "0-18", "19-30", "31-50", or "over 50".  You can have as many fields as you like.

The program starts by reading this data. Then it begins picking people at random and putting them into groups.  The program picks the "best" group by treating each property value as a "charge" and assuming that like charges repel each other.

The force of repulsion due to each particular property (say "m/f/o") of a person is equal to the number of other people in the group who have the same value for that property, divided by the number of possible values for that property (3 for the "m/f/o" example)[^1].  The total replsive force between a user and a group is the sum of the forces from all of the properties.  The program puts the user into the group that has the smallest repulsive force, and then repeats this process until all of the people in the input population are sorted into groups.

## Usage

The program is written in perl, and the usage syntax is:

    perl pdg.pl in.csv ngroups [out.csv]

If an output file is specified, the final group information is written into it. Otherwise, the program just sorts the people and prints histograms showing the distribution of properties in the input population and each of the created groups.

## Output

This repository contains a test file (in.csv) that you can experiment with.  The content of the file looks like this:

     User0,f,30-50,Bachelors,employed,mountain
     User1,f,0-18,Masters,employed,central
     User2,m,30-50,Doctorate,unemployed,pacific
     User3,f,over 50,HS,employed,central
     User4,f,30-50,Masters,employed,central
     ... etc.

If we ran pdg.pl on this file and sorted it into 3 groups, the output on the screen would start like this:

    TOTAL POPULATION: 101 students
    -----------------
    Property number 1:
             f | ########################################################## 57.4%
             m | ######################################## 39.6%
             o | ### 3.0%
    
    Property number 2:
          0-18 | ############################### 30.7%
         19-30 | ###################################### 37.6%
         30-50 | #################### 19.8%
       over 50 | ############ 11.9%
    
    Property number 3:
     Bachelors | ################################ 31.7%
     Doctorate | ########### 10.9%
            HS | ######################### 24.8%
       Masters | ########################### 26.7%
        non-HS | ###### 5.9%
    
    Property number 4:
      employed | #################################################################################### 83.2%
    unemployed | ################# 16.8%
    
    Property number 5:
       central | ############################## 29.7%
       eastern | ##################### 20.8%
      mountain | ######################## 23.8%
       pacific | ########################## 25.7%

and then would show statistics for each of the created groups, like this:

    GROUP POPULATIONS
    -----------------
    GROUP 0: 34 students
    Property number 1:
             f | ################### 55.9%
             m | ############# 38.2%
             o | ## 5.9%
    
    Property number 2:
          0-18 | ############ 35.3%
         19-30 | ############# 38.2%
         30-50 | ##### 14.7%
       over 50 | #### 11.8%
    
    Property number 3:
     Bachelors | ############ 35.3%
     Doctorate | ### 8.8%
            HS | ######## 23.5%
       Masters | ########## 29.4%
        non-HS | # 2.9%
    
    Property number 4:
        employed | ############################ 82.4%
      unemployed | ###### 17.6%
    
    Property number 5:
       central | ########## 29.4%
       eastern | ###### 17.6%
      mountain | ######## 23.5%
       pacific | ########## 29.4%
    ... etc., for other groups

The output file would contain data like this:

    GROUP 0,
    User1,f,0-18,Masters,employed,central,
    User3,f,over 50,HS,employed,central,
    User6,f,over 50,HS,employed,mountain,
    User8,m,0-18,Bachelors,unemployed,mountain,
    User10,m,over 50,Bachelors,employed,central,
    ...etc.
    GROUP 1,
    User2,m,30-50,Doctorate,unemployed,pacific,
    User9,f,30-50,Bachelors,employed,central,
    User11,m,19-30,HS,unemployed,mountain,
    User13,f,0-18,Bachelors,unemployed,eastern,
    User19,f,30-50,non-HS,employed,pacific,
    ...etc.
    
## Credits

I was inspired to take a look at this problem after looking at a script written by [Ethan Steere](https://www.linkedin.com/in/ethan-steere-ba8046222?trk=public_profile_browsemap).  Steere's script makes use of a [java implementation](https://grafo.etsii.urjc.es/optsicom/mdgp/mdgp_jors_2011.jar) of MDGP-solving methods provided by [Gallego, Laguna, Martí and Duarte](https://grafo.etsii.urjc.es/optsicom/mdgp).  At that time I hadn't heard of the MDG problem, and thought it would be interesting to try to come up with a simpler, more approximate solution that I could understand, and which might be useful in some situations.

[^1]: The number of possible values acts like a "coupling constant" for this propert's force.

