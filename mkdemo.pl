#!/usr/bin/perl

use strict;

my $properties = [
    ['m','m','m','m','f','f','f','f','f','o'],
    ['0-18','0-18','19-30','19-30','19-30','30-50','30-50','over 50'],
    ['non-HS','HS','HS','Bachelors','Bachelors','Bachelors','Masters','Masters','Doctorate'],
    ['employed','employed','employed','employed','employed','employed','unemployed'],
    ['eastern','central','mountain','pacific']
    ];

my $npeople = 101;

for ( my $i=0; $i<$npeople; $i++ ) {
    my $output = '';
    my $userid = "User$i";
    $output .= "$userid,";
    for ( my $j=0; $j<@{$properties}; $j++ ) {
	my $nval = @{$properties->[$j]};
	my $ival = int($nval*rand());
	$output .= "$properties->[$j][$ival],";
    }
    chop($output);
    print "$output\n";
}
