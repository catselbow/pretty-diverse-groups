#!/usr/bin/perl

# PDG = Pretty Diverse Groups
# Use attribute values as "charges" with like charges repelling each other.

use strict;

use Text::CSV;
use Data::Dumper;

my $infile = shift;
my $ngroups = shift;
my $outfile = shift;

die "Syntax: $0 in.csv ngroups [out.csv]\n" unless ( $infile && $ngroups );

my $debug = 0;

# $student_properties[$istudent][$iproperty]{value} = 0 or 1
my @student_properties = ();
my $nstudents = 0; # Number of students
my @picked = (); # Has this student been picked? 0 or 1
my @student_group = {}; # $student_group[$istudent] = $igroup

# $group_properties[$igroup][$iproperty]{value} = count of students in this group with that value.
my @group_properties = ();
my @group_count = (); # $group_count[$igroup] = Number of students in this group

# $property_values[$iproperty]{value} = total number of students with that value.
my @property_values = ();
my @nvalues = (); # $nvalues[$iproperty] = number of distinct values for that property.

# Ingest students:
# Note that, for simplicity, we treat column 0 (the userid) as just another
# property.  This shouldn't affect the results, since each student should
# have a unique userid, but things could be sped up if column zero were treated
# as a special case.
my $csv = Text::CSV->new( {binary=>1} );
open ( my $fh, '<', $infile ) or die "Can't open input file: $!\n";
while ( <$fh> ) {
    my $status = $csv->parse($_) or die "Error parsing line: ".$csv->error_diag()."\n";
    my @column = $csv->fields();
    for ( my $c=0; $c<@column; $c++ ) {
	$property_values[$c]{$column[$c]}++;
	$student_properties[$nstudents][$c]{$column[$c]}++;
    }
    $nstudents++;
}

my $studentspergroup = int($nstudents/$ngroups);
my $leftovers = int($nstudents - $studentspergroup*$ngroups);

print "Read $nstudents students.  Sorting into $ngroups groups.\n" if $debug;
print "Students per group = $studentspergroup\n" if $debug;
print "Leftovers = $leftovers\n" if $debug;

my $nproperties = @property_values;
print "There are $nproperties properties.\n" if $debug;

for ( my $iproperty=0; $iproperty<$nproperties; $iproperty++ ) {
    $nvalues[$iproperty] = scalar(keys %{$property_values[$iproperty]});
    print "Property $iproperty has $nvalues[$iproperty] values.\n" if $debug;
}

my $nprocessed = 0; # Number of students that have been sorted into groups.

while ( $nprocessed < $nstudents ) {

    print "Processed $nprocessed students...\n" if $debug;

    # Pick a student:
    my $istudent;
    do {
	$istudent = int($nstudents*rand());
    } while ( $picked[$istudent] );
    $picked[$istudent]++;
    
    # Find a group:
    my $minforce = -1;
    my $mingroup;
    for ( my $igroup=0; $igroup<$ngroups; $igroup++ ) {
	if ( $group_count[$igroup] < $studentspergroup ) {
	    my $force = 0;
	    for ( my $iproperty=0; $iproperty<$nproperties; $iproperty++ ) {
		for my $value (keys %{$property_values[$iproperty]}) {
		    $force +=
			$student_properties[$istudent][$iproperty]{$value} *
			$group_properties[$igroup][$iproperty]{$value} /
			$nvalues[$iproperty];
		}
	    }
	    if ( $minforce == -1 || $force < $minforce ) {
		$minforce = $force;
		$mingroup = $igroup;
	    }
	}
    }
    die ("ERROR: Bad force!\n")  if ( $minforce == -1 );
    
    print "Adding student $istudent to group $mingroup\n" if $debug;
    
    # Add student to group with min force:
    $student_group[$istudent] = $mingroup;
    
     # Update count of students in this group:
    $group_count[$mingroup]++;
    
    # Update group properties:
    for ( my $iproperty=0; $iproperty<$nproperties; $iproperty++ ) {
	for my $value (keys %{$property_values[$iproperty]}) {
	    $group_properties[$mingroup][$iproperty]{$value} +=
		$student_properties[$istudent][$iproperty]{$value};
	}
    }
    
    # Add to count of processed students:
    $nprocessed++;
    
    # Increase allowed group enrollment when we reach the "leftovers":
    $studentspergroup++ if ( $nprocessed == $studentspergroup*$ngroups );
}

print "group_properties:\n".Dumper(\@group_properties) if $debug;
print "property_values:\n".Dumper(\@property_values) if $debug;

print "TOTAL POPULATION: $nstudents students\n";
print "-----------------\n";
# Skip property number 0 (the userid):
for ( my $iproperty=1; $iproperty<$nproperties; $iproperty++ ) {
    print "Property number $iproperty:\n";
    for my $value ( sort keys %{$property_values[$iproperty]} ) {
	printf("%10s | ", $value);
	print '#'x$property_values[$iproperty]{$value}." ";
	my $percent = 100.0*$property_values[$iproperty]{$value}/$nstudents;
	printf( "%3.1f\%", $percent );
	print "\n";
    }
    print "\n";
}

print "GROUP POPULATIONS\n";
print "-----------------\n";
for ( my $igroup=0; $igroup<$ngroups; $igroup++ ) {
    print "GROUP $igroup: $group_count[$igroup] students\n";
    for ( my $iproperty=1; $iproperty<$nproperties; $iproperty++ ) {
	print "Property number $iproperty:\n";
	for my $value ( sort keys %{$property_values[$iproperty]} ) {
	    printf("%10s | ", $value);
	    print '#'x$group_properties[$igroup][$iproperty]{$value}." ";
	    my $percent = 100.0*$group_properties[$igroup][$iproperty]{$value}/$group_count[$igroup];
	    printf( "%3.1f\%", $percent );
	    print "\n";
	}
	print "\n";
    }
    print "\n";
}

if ( $outfile ) {
    print "Writing sorted students into $outfile.\n";
    open ( my $outfh, '>', $outfile ) or die "Can't open output file: $!\n";
    for ( my $igroup=0; $igroup<$ngroups; $igroup++ ) {
	print $outfh "GROUP $igroup,\n";
	for ( my $istudent=0; $istudent<$nstudents; $istudent++ ) {
	    if ( $student_group[$istudent] == $igroup ) {
		for ( my $iproperty=0; $iproperty<$nproperties; $iproperty++ ) {
		    my $value = (keys %{$student_properties[$istudent][$iproperty]})[0];
		    print $outfh "$value,";
		}
		print $outfh "\n";
	    }
	}
    }
    close ( $outfh);
}
